# Add More Alternate

## Introduction

The Add More Alternate module simplifies the `Add more` multiple cardinality handling so that it just has `Add item` and `Remove item` buttons. This only works for fields that are set to limited cardinality greater than 1 (i.e. not -1 or 1).

Once enabled, a `Use the add more alternate` checkbox will show up on the bottom of the form display page.

## Requirements

No special requirements.

## Installation

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8

## Maintainers

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
