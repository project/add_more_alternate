<?php

/**
 * @file
 * Contains add_more_alternate.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_help().
 */
function add_more_alternate_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.add_more_alternate':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A longer extended field label to replace the short label.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function add_more_alternate_preprocess_file_widget_multiple(&$variables) {
  if (isset($variables['table']) && isset($variables['element']['#add_more_alternate']) && $variables['element']['#add_more_alternate'] === TRUE) {
    add_more_alternate_remove_table_ordering($variables, TRUE);
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function add_more_alternate_preprocess_field_multiple_value_form(&$variables) {
  // Remove multiple value ordering.
  if (isset($variables['table']) && isset($variables['element']['#add_more_alternate']) && $variables['element']['#add_more_alternate'] === TRUE) {
    add_more_alternate_remove_table_ordering($variables);
  }
}

/**
 * Remove table reordering elements from variables.
 */
function add_more_alternate_remove_table_ordering(&$variables, $is_file = FALSE) {
  // Remove weight column.
  unset($variables['table']['#header'][1]);

  // Remove tabledrag actions.
  unset($variables['table']['#tabledrag']);

  foreach ($variables['table']['#rows'] as $row_id => $row) {
    // Remove draggable class.
    $variables['table']['#rows'][$row_id]['class'] = [];

    if ($is_file) {
      // Remove weight, leave operations.
      unset($variables['table']['#rows'][$row_id]['data'][1]);
    }
    else {
      // Remove weight and select.
      unset($variables['table']['#rows'][$row_id]['data'][2]);
      unset($variables['table']['#rows'][$row_id]['data'][0]);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function add_more_alternate_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['#entity_builders'])) {
    $form_display = $form_state->getStorage()['form_display'];
    $use_add_more_alternate = $form_display->getThirdPartySetting('add_more_alternate', 'use_add_more_alternate', '');
    if ($use_add_more_alternate === 1) {
      $user = \Drupal::currentUser();
      $roles = $user->getRoles();
      $config = \Drupal::configFactory()->get('add_more_alternate.settings');

      if (in_array('administrator', $roles) && $config->get('admin_disable') === 1) {
        return;
      }

      $required = FALSE;
      foreach (Element::children($form) as $field_name) {
        $cardinality = -1;
        if (array_key_exists('widget', $form[$field_name]) &&
          array_key_exists('0', $form[$field_name]['widget']) &&
          array_key_exists('#cardinality', $form[$field_name]['widget'][0])) {
          $cardinality = $form[$field_name]['widget'][0]['#cardinality'];
        }
        elseif (array_key_exists('widget', $form[$field_name]) &&
          array_key_exists('#cardinality', $form[$field_name]['widget'])) {
          $cardinality = $form[$field_name]['widget']['#cardinality'];
        }

        if ($cardinality > 1) {
          $form[$field_name]['widget']['#add_more_alternate'] = TRUE;
          $required = TRUE;
        }
      }

      if ($required) {
        $form['#attached']['drupalSettings']['add_more_alternate']['add_item_label'] = $config->get('add_item_label');
        $form['#attached']['drupalSettings']['add_more_alternate']['add_item_classes'] = $config->get('add_item_classes');
        $form['#attached']['drupalSettings']['add_more_alternate']['remove_item_label'] = $config->get('remove_item_label');
        $form['#attached']['drupalSettings']['add_more_alternate']['remove_item_classes'] = $config->get('remove_item_classes');
        $form['#attached']['library'][] = 'add_more_alternate/forms';
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function add_more_alternate_form_entity_form_display_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $display = $form_state->getFormObject()->getEntity();
  $use_add_more_alternate = $display->getThirdPartySetting('add_more_alternate', 'use_add_more_alternate', 0);

  $form['use_add_more_alternate'] = [
    '#type' => 'checkbox',
    '#title' => t('Use the add more alternate'),
    '#description' => t('Check this box to enable using the <em>add more</em> alternate widget where applicable.'),
    '#default_value' => $use_add_more_alternate,
  ];

  $form['actions']['submit']['#submit'][] = 'add_more_alternate_form_display_submit';
}

/**
 * Custom form display submit handler.
 */
function add_more_alternate_form_display_submit(array &$form, FormStateInterface $form_state) {
  $use_add_more_alternate = $form_state->getValue('use_add_more_alternate');
  $form_display = $form_state->getFormObject()->getEntity();
  $form_display->setThirdPartySetting('add_more_alternate', 'use_add_more_alternate', $use_add_more_alternate)->save();
}
